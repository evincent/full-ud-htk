This page provide HTK patches for uncertainty decoding with a full uncertainty covariance matrix. This work is based on previous work from [Ramon Astudillo](http://www.astudillo.com/ramon/research/stft-up/).

# Licence and citation
This software was authored by Yann Salaün and Emmanuel Vincent (Inria) and is distributed under the terms of the [modified BSD license](license.txt).

However, it should be noted that once you apply the patch to the HTK source code, you must obey the [HTK license](http://htk.eng.cam.ac.uk/docs/license.shtml).

If you use this software in a publication, please cite
>  Dung T. Tran, Emmanuel Vincent, and Denis Jouvet, [Extension of uncertainty propagation to dynamic MFCCs for noise robust ASR](https://hal.inria.fr/hal-00954654/en), in Proc. ICASSP, pp. 5507-5511, 2014.

# Download
* [htk_UD_full.tar.gz](files/htk_UD_full.tar.gz)

# Build
* Get HTK
* Untar the patches and patch the code


	tar xzf HTK_UD_full.tar.gz
	patch htk/HTKLib/HMath.c htk_UD_full/HMath.patch
	patch htk/HTKLib/HModel.c htk_UD_full/HModel.patch
	patch htk/HTKLib/HParm.c htk_UD_full/HParm.patch
	patch htk/HTKLib/HWave.c htk_UD_full/HWave.patch

* Build HTK as usual


	cd htk
	./configure
	make

* The ./HTKTools/HVite executable you obtain is a modified version which takes features with full uncertainty as input. `HVite -V` should return version number with 'full' suffixes.

# Generate features with full uncertainty

We use to generate the feature with the `writehtk` MATLAB function in the [voicebox toolbox](http://www.ee.ic.ac.uk/hp/staff/dmb/voicebox/doc/voicebox/). Our patch supposes that the covariance matrix is reshaped to a vector and concatenated to the mean vector.

# Results
See above publication.

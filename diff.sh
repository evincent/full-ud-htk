#!/bin/bash

mkdir htk_UD_full
for file in {HMath,HModel,HParm,HWave}
do
	diff htk_bak/HTKLib/$file.c htk/HTKLib/$file.c > htk_UD_full/$file.patch
done
tar czf htk_UD_full.tar.gz htk_UD_full
